<!-- ======================================================================= -->
<!--                                                                         -->
<!--                                                          /   /   \      -->
<!--   Made By IsCoffeeTho                                  /    |      \    -->
<!--                                                       |     |       |   -->
<!--   README.md                                           |      \      |   -->
<!--                                                       |       |     |   -->
<!--   Last Edited: 08:22PM 02/06/2023                      \      |    /    -->
<!--                                                          \   /   /      -->
<!--                                                                         -->
<!-- ======================================================================= -->

# BitBurner AdvSH

![aSH Term aSH Terminal v1.0.0 BitBurner v2.3.0 (79bd057aa) user@aSH:~$](https://cdn.discordapp.com/attachments/543989987666952212/1114138164978782268/image.png)

This project is an attempt to add unix functionality to your bitburner gameplay

```sh
# Installation

wget https://gitlab.com/iscoffeetho/advsh/-/raw/main/installer.js installer.js;
./installer.js
```
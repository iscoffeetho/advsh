/* ========================================================================== */
/*                                                                            */
/*                                                             /   /   \      */
/*   Made By IsCoffeeTho                                     /    |      \    */
/*                                                          |     |       |   */
/*   shell.js                                               |      \      |   */
/*                                                          |       |     |   */
/*   Last Edited: 08:15PM 02/06/2023                         \      |    /    */
/*                                                             \   /   /      */
/*                                                                            */
/* ========================================================================== */

/*       ___ _  _   _____
 *  __ _/ __| || |    |  ___  . _  . _  _
 * / _` \__ \ __ |    | /___/ |/ \ |/ \/ \
 * \__,_|___/_||_|    | \___, |    |     |
*/

import { masterProcess } from 'sys/process';
import { terminal } from 'sys/terminal';
import { fs } from 'sys/fs';

const versionMajor = 1;
const versionMinor = 0;
const versionPatch = 0;

/** @param {NS} ns */
export async function main(ns) {
    var procs = new masterProcess();
    procs.setGlobalNS(ns);
    var game = ns.ui.getGameInfo();
    var hostname = "aSH";
    var term = new terminal(ns);
    var filesys = new fs();
    filesys.hookNS(ns);

    var restart = false;

    ns.atExit(() => {
        term.stop();
        if (restart)
        {
            var doc = eval('document');
            while (!doc.getElementById("terminal-input"))
                ns.asleep(1);
            const tmin = doc.getElementById("terminal-input");
            tmin.value = `./${ns.getScriptName()};`;
            const tminhandle = Object.keys(tmin)[1];
            tmin[tminhandle].onChange({target:tmin});
            tmin[tminhandle].onKeyDown({key:'Enter',preventDefault:()=>null});
        }
    });

    var motd = () => {
        term.log(`\x1b[38;5;250;1m      ___ _  _   \x1b[97m_____\x1b[0m`);
        term.log(`\x1b[38;5;248;1m __ _/ __| || |  \x1b[97m  |  ___  . _  . _  _\x1b[0m`);
        term.log(`\x1b[38;5;246;1m/ _\` \\__ \\ __ |  \x1b[97m  | /___/ |/ \\ |/ \\/ \\\x1b[0m`);
        term.log(`\x1b[38;5;244;1m\\__,_|___/_||_|  \x1b[97m  | \\___, |    |     |\x1b[0m`);
        term.log(`\x1b[93;1maSH Terminal \x1b[94mv${versionMajor}.${versionMinor}.${versionPatch}\x1b[0m`);
        term.log(`\x1b[93;1mBitBurner \x1b[94mv${game.version} (${game.commit})\x1b[0m`);
    };

    var commands = {
		"help": () => {
			for (var entry in commands)
			{
				term.log(entry);
			}
		},
        "proc": () => {
            var proccesses = procs.enumerable();
            for (var i = 0; i < proccesses.length; i++)
                term.log(proccesses[i].name);
        },
        "reload": () => {
            restart = true;
            ns.exit();
        },
        "motd": () => {
            motd();
        },
        "exit": () => {
            ns.exit();
        },
        "echo": (...msg) => {
            term.log(...msg);
        },
        "cls": () => {
            commands["clear"]();
        },
        "clear": () => {
            term.clear();
            motd();
        },
        "pwd": () => {
            term.log(filesys.cwd());
        },
        "cwd": () => {
            commands["pwd"]();
        },
        "nano": (...args) => {
            var path = [];
            var flgs = [];
            args.forEach((v) => {
                if (v[0] == '-')
                    flgs.push(v);
                else
                    path.push(v);
            });
            path = filesys.path(path[0]);
            term.log(path);
            term.execute(`nano ${path.slice(1)}`);
        },
        "vim": (...args) => {
            var path = [];
            var flgs = [];
            args.forEach((v) => {
                if (v[0] == '-')
                    flgs.push(v);
                else
                    path.push(v);
            });
            path = filesys.path(path[0]);
            term.log(path);
            term.execute(`vim ${path.slice(1)}`);
        }
    };

    filesys.cd("~");
    motd();
    while (1) {
        var cwd = filesys.cwd({replaceHomeWithTilde:true});
        var promptText = `\x1b[93;1muser@${hostname}\x1b[0m:\x1b[94m${cwd}\x1b[0m\$`;
        var userin = await term.input(promptText);
        term.log(`${promptText} ${userin}`);

        var cmdBuffer = [];
        userin.replace(/(([^"]+("[^"]*"[^;"]*)*);|([^"]+("[^"]*"[^"]*)*)$)/g, (m) => {
            if (m.at(-1) == ';')
                m = m.slice(0, -1);
            cmdBuffer.push(m);
        });

        for (var i = 0; i < cmdBuffer.length; i++)
        {
            var argVector = [];
            cmdBuffer[i].replace(/("[^"]+"|[^" ]+)+/g, (m) => {
                argVector.push(m.replace(/"/g, ''));
            });
            
            if (argVector.length == 0) return;
            if (argVector[0].startsWith("./") || argVector[0].startsWith("/")) {
                // term.log(filesys.path(argVector[0]));
                term.log("Running scripts is still underworks");
                term.log("for more info go to https://gitlab.com/iscoffeetho/advsh/-/issues/1");
            }
            else if (commands[argVector[0]])
            {
                try {
                    await commands[argVector.shift()](...argVector);
                }
                catch (error)
                {
                    term.log('Uncaught Error', error);
                    term.log('');
                    term.log(error.stack);
                    term.log('');
                }
            }
            else
                term.log(`Unknown command ${argVector[0]}`);
        }
    }
}
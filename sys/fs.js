/* ========================================================================== */
/*                                                                            */
/*                                                             /   /   \      */
/*   Made By IsCoffeeTho                                     /    |      \    */
/*                                                          |     |       |   */
/*   fs.js                                                  |      \      |   */
/*                                                          |       |     |   */
/*   Last Edited: 07:51PM 02/06/2023                         \      |    /    */
/*                                                             \   /   /      */
/*                                                                            */
/* ========================================================================== */

var gNS;
var gFS;

export function fs () {
    /** @param {NS} ns*/
    this.hookNS = (ns) => {
        gNS = ns;
        gFS = {};
    }

    var cwd = [];
    var home = ["home"];

    this.cwd = (opts={
        replaceHomeWithTilde:false
    }) => {
        var cwdc = Array.from(cwd);
        if (opts)
        {
            if (opts.replaceHomeWithTilde)
            {
                var doesMatch = true;
                for (var i = 0; i < home.length; i++)
                {
                    if (cwdc[i] != home[i])
                    {
                        doesMatch = false;
                        break;
                    }
                }
                if (doesMatch)
                {
                    for (var i = 0; i < home.length; i++)
                        cwdc.shift();
                    cwdc.unshift('~');
                }
            }
        }
        if (cwdc.at(0) != '~')
            return `/${cwdc.join('/')}`;
        return `${cwdc.join('/')}`;
    }

    this.setHome = (home) => {
        home = this.parsePath(home);
    }

    this.cd = (path) => {
        cwd = this.parsePath(path);
    }

    this.path = (path) => {
        return `/${this.parsePath(path).join("/")}`;
    }

    this.parsePath = (path) => {
        var parsed = [""];
        path = path.split("/");
        if (path.length == 0) Array.from(cwd);

        if (path[0] == '') // only occurs when path starts with '/'
            parsed = []; // go to root
        else 
            parsed = Array.from(cwd);
        
        for (var i = 0; i < path.length; i++)
        {
            if (path[i] == '') continue;
            if (path[i] == '.') continue;
            if (path[i] == '..')
                parsed.pop();
            else if (path[i] == '~')
                parsed = Array.from(home);
            else
                parsed.push(path[i]);
        }
        return parsed;
    }

    this.readFile = (path, callback) => {
        callback(null, new Error("File does not exist"));
    }

    this.cleanPath = (path) => {
        return path;
    }

    return this;
}
/* ========================================================================== */
/*                                                                            */
/*                                                             /   /   \      */
/*   Made By IsCoffeeTho                                     /    |      \    */
/*                                                          |     |       |   */
/*   terminal.js                                            |      \      |   */
/*                                                          |       |     |   */
/*   Last Edited: 07:51PM 02/06/2023                         \      |    /    */
/*                                                             \   /   /      */
/*                                                                            */
/* ========================================================================== */

/*
    TODO:

    add more terminal keybinds

*/
var doc = eval('document');

const termID = "ashTerm";

/** @param {NS} ns */
export function terminal(ns) {
    var tty = [];
    this.maxEntries = 500;
    this.terminalWait = 10;

    var inBuff = [];
    var inHistory = [];
    var historyPointer = 0;
    var currIn = "";
    var currPrompt = "";
    var privacyMode = false;
    var canRender = false;

    this.stop = () => {
        if (!doc.getElementById("terminal")) return;
        doc.getElementById(termID).remove();
        doc.getElementById("terminal").style.display = 'block';
        var promptElem = doc.getElementById("terminal-input").parentElement.querySelector("p");
        var inputElem = doc.getElementById("terminal-input");
        promptElem.attributes.removeNamedItem("style");
        promptElem.innerHTML = `[home&nbsp;~]&gt;&nbsp;`;
        inputElem.attributes.removeNamedItem("style");
        inputElem.parentElement.attributes.removeNamedItem("style");
        inputElem.onkeydown = undefined;
    };

    var renderLine = (line) => {
        if (line.length == 0)
            line = '&nbsp;';
        if (doc.getElementById(termID))
            doc.getElementById(termID).insertAdjacentHTML("beforeend", `<li style="color:white;white-space:pre;font-family:monospace;">${line}</li>`);
    }

    var renderTerminal = async () => {
        if (!doc.getElementById("terminal")) return;
        if (doc.getElementById(termID)) return;
        canRender = true;
        doc.getElementById("terminal").insertAdjacentHTML("afterend", `<ul id="${termID}"></ul>`);
        doc.getElementById(termID).classList = doc.getElementById("terminal").classList
        doc.getElementById("terminal").style.display = 'none';
        doc.getElementById(termID).style.color = "white";
        doc.getElementById(termID).style.whiteSpace = "pre";
        doc.getElementById(termID).style.fontFamily = "monospace";
        doc.getElementById(termID).style.backgroundColor = "black";
        doc.getElementById(termID).style.fontSize = "13px";
        doc.getElementById("terminal-input").style.color = "white";
        doc.getElementById("terminal-input").style.whiteSpace = "pre";
        doc.getElementById("terminal-input").style.fontFamily = "monospace";
        doc.getElementById("terminal-input").style.fontSize = "13px";
        doc.getElementById("terminal-input").style.paddingTop = "0px";
        doc.getElementById("terminal-input").style.paddingBottom = "0px";
        if (privacyMode)
            doc.getElementById("terminal-input").type = 'password';
        doc.getElementById("terminal-input").value = currIn;
        doc.getElementById("terminal-input").parentElement.style.backgroundColor = "black";
        var promptElem = doc.getElementById("terminal-input").parentElement.querySelector("p")
        promptElem.style.color = "white";
        promptElem.style.whiteSpace = "pre";
        promptElem.style.fontFamily = "monospace";
        promptElem.style.fontSize = "13px";
        promptElem.innerHTML = currPrompt;

        doc.getElementById("terminal-input").onkeydown = (e) => {

            // todo: add terminal keybinds like interrupt

            if (e.code == 'Enter') {
                inBuff.push(doc.getElementById("terminal-input").value);
                if (doc.getElementById("terminal-input").value != inHistory[0]) // save into command history if different than last command
                    inHistory.unshift(doc.getElementById("terminal-input").value);
                currIn = "";
                doc.getElementById("terminal-input").value = currIn;
                e.preventDefault();
                e.cancelBubble = true;
                if (e.stopImmediatePropagation)
                    e.stopImmediatePropagation();
                historyPointer = 0;
            }
            else if (e.code == 'ArrowDown') {
                if (historyPointer == 0)
                    doc.getElementById("terminal-input").value = "";
                else
                    doc.getElementById("terminal-input").value = inHistory[--historyPointer];
                e.preventDefault();
                e.cancelBubble = true;
                if (e.stopImmediatePropagation)
                    e.stopImmediatePropagation();
            }
            else if (e.code == 'ArrowUp') {
                if (inHistory.length > 0)
                    doc.getElementById("terminal-input").value = inHistory[historyPointer];
                if (historyPointer < inHistory.length - 1)
                    historyPointer++
                e.preventDefault();
                e.cancelBubble = true;
                if (e.stopImmediatePropagation)
                    e.stopImmediatePropagation();
            }
            setTimeout(() => {
                currIn = doc.getElementById("terminal-input").value;
            }, 1);
        };

        for (var i = 0; i < tty.length; i++)
            renderLine(tty[i]);
    };

    var ansiStyle = {

    };

    var ansiVGAColorFn = (n) => {
        return [
            "#000",
            "#A00",
            "#0A0",
            "#AA0",
            "#00A",
            "#A0A",
            "#0AA",
            "#AAA",
            "#555",
            "#f55",
            "#5f5",
            "#ff5",
            "#55f",
            "#f5f",
            "#5ff",
            "#fff"
        ][n];
    }

    var ansi256ColorFn = (n) => {
        if (n < 16) {
            return [
                "#000",
                "#800",
                "#080",
                "#880",
                "#008",
                "#808",
                "#088",
                "#ccc",
                "#888",
                "#f00",
                "#0f0",
                "#ff0",
                "#00f",
                "#f0f",
                "#0ff",
                "#fff"
            ][n];
        }
        else if (n < 232) {
            n -= 16;
            var b = n % 6;
            n /= 6;
            n = parseInt(n);
            var g = n % 6;
            n /= 6;
            n = parseInt(n);
            var r = n % 6;
            r *= 255 / 5;
            g *= 255 / 5;
            b *= 255 / 5;
            r = r.toString(16).padStart(2, '0');
            g = g.toString(16).padStart(2, '0');
            b = b.toString(16).padStart(2, '0');
            return `#${r}${g}${b}`;
        } else if (n < 256) {
            n -= 232;
            n /= 24;
            n *= 256;
            n = parseInt(n);
            n = n.toString(16).padStart(2, '0');
            return `#${n}${n}${n}`;
        }
        return "#000";
    }

    var filterText = (txt) => {
        if (txt.length == 0)
            return '&nbsp;';

        var elems = [];
        txt = txt
            .replace(/&/g, "&amp;")
            .replace(/>/g, "&gt;")
            .replace(/</g, "&lt;");
        var i = 0;
        var buffer = "<span>";

        while (i < txt.length) {
            if (txt[i] == '\x1b') {
                i++;
                if (txt[i] != '[') continue;
                i++;
                var attr = [];
                var currAttr = 0;
                while (txt.charCodeAt(i) >= '0'.charCodeAt(0) && txt.charCodeAt(i) <= '9'.charCodeAt(0)) {
                    currAttr *= 10;
                    currAttr += txt.charCodeAt(i) - '0'.charCodeAt(0);
                    if (txt[++i] == ';') { // continue to next attribute
                        attr.push(currAttr);
                        currAttr = 0;
                        i++;
                    }
                }
                if (txt[i++] != 'm') continue;
                attr.push(currAttr);
                for (var j = 0; j < attr.length; j++) {
                    // for each ansi attr at position
                    if (attr[j] == 0) {
                        ansiStyle = {};
                        continue;
                    }
                    switch (attr[j]) {
                        case 1:
                            ansiStyle.weight = 700;
                            break;
                        case 2:
                            ansiStyle.weight = 200;
                            break;
                        case 3:
                            ansiStyle.italics = true;
                            break;
                        case 4:
                            ansiStyle.underline = true;
                            break;
                        case 7:
                            ansiStyle.invert = true;
                            break;
                        case 9:
                            ansiStyle.strikethrough = true;
                            break;
                        case 22:
                            delete ansiStyle.weight;
                            break;
                        case 23:
                            ansiStyle.italics = false;
                            break;
                        case 24:
                            ansiStyle.underline = false;
                            break;
                        case 27:
                            ansiStyle.invert = false;
                            break;
                        case 29:
                            ansiStyle.strikethrough = false;
                            break;
                        case 30:
                        case 31:
                        case 32:
                        case 33:
                        case 34:
                        case 35:
                        case 36:
                        case 37:
                            ansiStyle.fgColor = ansiVGAColorFn(attr[j] - 30);
                            break;
                        case 90:
                        case 91:
                        case 92:
                        case 93:
                        case 94:
                        case 95:
                        case 96:
                        case 97:
                            ansiStyle.fgColor = ansiVGAColorFn(attr[j] - 82);
                            break;
                        case 38:
                            if (attr[j + 1] == 5) {
                                if (j + 2 >= attr.length)
                                    break;
                                j += 2;
                                ansiStyle.fgColor = ansi256ColorFn(attr[j]);
                            }
                            else if (attr[j + 1] == 2) {
                                if (j + 4 >= attr.length)
                                    break;
                                var r = attr[j + 2];
                                var g = attr[j + 3];
                                var b = attr[j + 4];
                                r = r.toString(16).padStart(2, '0');
                                g = g.toString(16).padStart(2, '0');
                                b = b.toString(16).padStart(2, '0');
                                ansiStyle.fgColor = `#${r}${g}${b}`;
                            }
                            break;
                        case 39:
                            delete ansiStyle.fgColor;
                            break;
                        case 40:
                        case 41:
                        case 42:
                        case 43:
                        case 44:
                        case 45:
                        case 46:
                        case 47:
                            ansiStyle.bgColor = ansiVGAColorFn(attr[j] - 40);
                            break;
                        case 100:
                        case 101:
                        case 102:
                        case 103:
                        case 104:
                        case 105:
                        case 106:
                        case 107:
                            ansiStyle.bgColor = ansiVGAColorFn(attr[j] - 92);
                            break;
                        case 48:
                            if (attr[j + 1] == 5) {
                                if (j + 2 >= attr.length)
                                    break;
                                j += 2;
                                ansiStyle.bgColor = ansi256ColorFn(attr[j]);
                            }
                            else if (attr[j + 1] == 2) {
                                if (j + 4 >= attr.length)
                                    break;
                                var r = attr[j + 2];
                                var g = attr[j + 3];
                                var b = attr[j + 4];
                                r = r.toString(16).padStart(2, '0');
                                g = g.toString(16).padStart(2, '0');
                                b = b.toString(16).padStart(2, '0');
                                ansiStyle.bgColor = `#${r}${g}${b}`;
                            }
                            break;
                        case 49:
                            delete ansiStyle.bgColor;
                            break;
                        default:
                            break;
                    }
                }
                buffer += "</span>";
                elems.push(buffer);
                buffer = `<span style="`;

                // do ansi dance
                // console.log("");
                if (ansiStyle.weight)
                    buffer += `font-weight:${ansiStyle.weight};`;
                if (ansiStyle.italics)
                    buffer += `font-weight:${ansiStyle.weight};`;
                if (ansiStyle.underline)
                    buffer += `border-bottom:1px solid;margin-bottom:-1px;`;
                if (ansiStyle.strikethrough)
                    buffer += `text-decoration:line-through;`;
                if (ansiStyle.invert) {
                    buffer += `color:${ansiStyle.bgColor || `#000`};border-color:${ansiStyle.bgColor || `#000`};background-color:${ansiStyle.fgColor || `#fff`};`;
                }
                else {
                    if (ansiStyle.fgColor)
                        buffer += `color:${ansiStyle.fgColor};border-color:${ansiStyle.fgColor};`;
                    if (ansiStyle.bgColor)
                        buffer += `background-color:${ansiStyle.bgColor};`;
                }
                buffer += `">`;
                continue;
            }
            buffer += txt[i];
            i++;
        }
        buffer += "</span>";
        elems.push(buffer);
        // console.log(elems);
        return elems.join("");
    }

    (async () => {
        while (1) {
            while (!doc.getElementById("terminal"))
                await ns.asleep(this.terminalWait);
            await renderTerminal();
            while (doc.getElementById("terminal"))
                await ns.asleep(this.terminalWait);
            canRender = false;
        }
    })(); // makes sure that terminal is up to date

    this.print = (strs) => {
        strs = `${strs}`;
        strs = filterText(strs);
        tty.push(strs);
        if (tty.length > this.maxEntries)
            tty.shift();

        if (canRender)
            renderLine(strs);
    }

    this.log = (...msg) => {
        var line = [];
        for (var i = 0; i < msg.length; i++) {
            if (msg[i] instanceof Error)
            {
                line.push(`${msg[i].name}${msg[i].message ? `: ${msg[i].message}` : ''}`);
                continue;
            }
            switch (typeof msg[i]) {
                case 'object':
                    line.push(JSON.stringify(msg[i], null, "    "));
                    break;
                case 'function':
                    line.push(`[Function ${msg[i].name}]`);
                    break;
                default:
                    line.push(`${msg[i]}`);
                    break;
            }
        }
        this.print(line.join(" "));
    }

    this.setPrivacyMode = () => {
        privacyMode = true;
        if (canRender)
            doc.getElementById("terminal-input").type = 'password';
    }

    this.input = async (prompt) => {
        currPrompt = filterText(prompt + " ");
        if (doc.getElementById("terminal-input"))
            doc.getElementById("terminal-input").parentElement.querySelector("p").innerHTML = currPrompt;
        return new Promise(async (res, rej) => {
            while (inBuff.length == 0)
                await ns.asleep(this.terminalWait);
            privacyMode = false;
            doc.getElementById("terminal-input").type = '';
            res(inBuff.shift());
        });
    }

    this.execute = (command) =>
    {
        if (!doc.getElementById("terminal-input"))
            ns.toast("The Shell requires you to open the terminal immediately.");
        while (!doc.getElementById("terminal-input"))
            ns.asleep(1);
        const tmin = doc.getElementById("terminal-input");
        tmin.value = command;
        const tminhandle = Object.keys(tmin)[1];
        tmin[tminhandle].onChange({target:tmin});
        tmin[tminhandle].onKeyDown({key:'Enter',preventDefault:()=>null});
    }

    this.clear = () => {
        tty = [];
        if (canRender)
            doc.getElementById(termID).innerHTML = "";
    }
}
/* ========================================================================== */
/*                                                                            */
/*                                                             /   /   \      */
/*   Made By IsCoffeeTho                                     /    |      \    */
/*                                                          |     |       |   */
/*   process.js                                             |      \      |   */
/*                                                          |       |     |   */
/*   Last Edited: 07:51PM 02/06/2023                         \      |    /    */
/*                                                             \   /   /      */
/*                                                                            */
/* ========================================================================== */

import {events} from 'sys/events';

var gNS;
var gPID;

export function masterProcess() {
    var __events = new events();
    this.on = __events.on;
    this.once = __events.once;
    this.emit = __events.emit;

    var procs = [this];

    this.setGlobalNS = (ns) => {
        gNS = ns;
    };

    this.getGlobalNS = () => {
        return gNS
    };

    this.createProcess = () => {
        
    }

    this.enumerable = () => {
        
    }

    return this;
}

export function process()
{
    
}
/* ========================================================================== */
/*                                                                            */
/*                                                             /   /   \      */
/*   Made By IsCoffeeTho                                     /    |      \    */
/*                                                          |     |       |   */
/*   events.js                                              |      \      |   */
/*                                                          |       |     |   */
/*   Last Edited: 07:51PM 02/06/2023                         \      |    /    */
/*                                                             \   /   /      */
/*                                                                            */
/* ========================================================================== */

/**
 *  EventEmitter
 */
export function events() {
    var __on = {};
    var __once = {};

    this.on = (e = "", f = () => { }) => {
        if (__on[e] == undefined)
            __on[e] = [];
        __on[e].push(f);
    }

    this.once = (e = "", f = () => { }) => {
        if (__once[e] == undefined)
            __once[e] = [];
        __once[e].push(f);
    }

    this.emit = async (event = "", arg) => {
        if (__on[event] != undefined)
            for (var f in __on[event])
                await __on[event][f](arg);
        if (__once[event] != undefined)
            for (var f in __once[event])
                await __once[event][f](arg);
        delete __once[event];
    }

    return this;
}
/* ========================================================================== */
/*                                                                            */
/*                                                             /   /   \      */
/*   Made By IsCoffeeTho                                     /    |      \    */
/*                                                          |     |       |   */
/*   installer.js                                           |      \      |   */
/*                                                          |       |     |   */
/*   Last Edited: 08:23PM 02/06/2023                         \      |    /    */
/*                                                             \   /   /      */
/*                                                                            */
/* ========================================================================== */

/** @param {NS} ns */
export async function main(ns) {
	var doc = eval('document');

    var dl = async (file) => {
		await ns.tprint(`Downloading ${file}`);
        await ns.wget(`https://gitlab.com/iscoffeetho/advsh/-/raw/main/${file}`, file, "home")
    }
	
    await dl('sys/events.js');
    await dl('sys/fs.js');
    await dl('sys/process.js');
    await dl('sys/shell.js');
    await dl('sys/terminal.js');
    if (await ns.prompt("Finished Installing, would you like to enter aSH?", {type: "boolean"}))
    {
        if (!doc.getElementById("terminal-input"))
            ns.toast("The Shell requires you to open the terminal immediately.");
        while (!doc.getElementById("terminal-input"))
            ns.asleep(1);
        const tmin = doc.getElementById("terminal-input");
        tmin.value = './sys/shell.js';
        const tminhandle = Object.keys(tmin)[1];
        tmin[tminhandle].onChange({target:tmin});
        tmin[tminhandle].onKeyDown({key:'Enter',preventDefault:()=>null});
    }
}